<!-- TITLE: Začátek Dobrodružství -->
<!-- SUBTITLE: Začátek Galaktického Dobrodružství -->

# ResourcePack
Pro úžasný žážitek máme pro vás Resource Pack! Tento Resource Pack přidá texturu skafandrů.
Je jednoduché si ho aktivovat, stačí napsat /space a kliknout na "Texture Pack" (Item Frame)

# Upgradování Skafandrů
Potřebujeme si postavit tuto věc
![Galactic](/uploads/galactic-basic/galactic.png "Galactic")

Až tuto věc postavíme, tak dojdeme na Emerald Block a zmačkneme Shift a otevře se nám menu. V tomto menu klikneme na helmu a otevře se nám nákup Armoru.
![Galactic 1](/uploads/galactic-basic/galactic-1.png "Galactic 1")
![Galactic 2](/uploads/galactic-basic/galactic-2.png "Galactic 2")

# Multiblocks
## Airlocks (Udržuje vzduch)
[video](https://youtu.be/Wtjw9Q6Mjgk){.youtube}

## Repulsion Barrier
Tento item se chová jako Jump Pad, lze ho postavit takto:
![Sponge](/uploads/galactic-basic/sponge.png "Sponge")

## Modular Workbench
![Modularworkbench](/uploads/galactic-basic/modularworkbench.png "Modularworkbench")
