<!-- TITLE: Zakladní Informace -->
<!-- SUBTITLE: Vše o základu SlimeFunu -->

# SlimeFun - Informace o samotném pluginu
Slimefun je plugin, ktorý do minecraftu prináša nové plodiny, nástroje, materiály a mnoho ďalších vecí. Hráčom tak otvára nové investovanie xp, materiálu a hlavne veľa zábavy.

# SlimeFun Guide
Po registrácií a prihlásením na server dostanete automaticky Slimefun Guide, ktorú potrebujete pri práci so Slimefunom. Ak ste túto knihu stratili tak stačí otvoriť chat a napísať /sf guide. Kliknutím pravým tlačítkom na myši s knihou v ruke sa vám otvorí Slimefun Menu, kde sa vám zobrazujú rôzne kategórie od zbraní až po zložitejšie prístroje. Ľavým kliknutím v menu vyberete kategóriu / predmet, ktorý chcete zobraziť. Hore sa vám objaví kniha, ktorou sa sa vrátite späť. 

![Sf 1](/uploads/sf-basic/sf-1.png "Sf 1")
*Pokud knihu stratíš, můžeš jí získat zpět pomocí příkazu **/sf guide***

# Machines
K výrobě skoro každého předmětu ve SlimeFunu potřebuješ Machine. Machine uděláš tak, že jí postavíš podle receptu.
![Sf 3](/uploads/sf-basic/sf-3.png "Sf 3")

Poté to bude vypadat takto:
![Sf 2](/uploads/sf-basic/sf-2.png "Sf 2")

Příklad použití:
Budeme si chtít udělat Grandpas Walking Stick... Potřebujeme Enchanced Crafting Table, kteřý již máme postavený.
Máme tentop recept:
![Screenshot 3](/uploads/sf-basic/screenshot-3.png "Screenshot 3")

Do Dispenseru dáme potřebné itemy podle receptu
![Sf 5](/uploads/sf-basic/sf-5.png "Sf 5")

Poté klikneme na blok nad Machine (Crafting Table) a v Dispenseru se nám objeví item!
![Screenshot 4](/uploads/sf-basic/screenshot-4.png "Screenshot 4")

# Kategorie
## Basic Machines
Kategória, ktorá obsahuje rôzne stroje, ktoré majú pomerne jednoduché funkcie, a nevyžadujú na žiadny stroj energiu.

## Weapons
Kategória zbraní, ktorá obsahuje rôzne itemy, ktoré môžete použiť v PVP alebo PVE.

## Items 
Kategória plná pomocných vecí, ktoré majú rôzne funkcie, ako napríklad Backpack, Pomôcky alebo zdravotnícke potreby

## Tools
Kategória, ktorá obsahuje rôzne nástroje uľahčovania života

## Resource 
Kategória, ktorá zahŕňa základné materiály pre crafting Slimefun vecí ako napríklad ingoty, dusty atd.

## Food 
Kategória plná potravín, obsahuje rôzne potraviny a aj džúsy.

## Magical Items 
Kategória plná kúzelných predmetov, ktorá obsahuje magické craftovacie komponenty.

## Magical Armor 
Kategória plná kúzelného armoru, ktorý vám dovolí vysoko skákať, teleportovať sa bez zranenia atd.

## Technical Components 
Kategória obsahuje komponenty pre radu prístrojov / strojov, tak isto ako niekoľko dôležitých súčastí pre niektoré automatizované systémy

## Miscellaneous 
Kategória plná rôzneho obsahu, ktorá vám pomôže pri vyrábaní jedla / komponentov.

## Armor 
Kategória, ktorá pridáva rôzne druhy armoru, každá sada armoru musí byť vytvorená v Armor Forge

## Talismans 
Kategória, ktorá obsahuje rôzne Talizmany, z ktorých si môžete vybrať. Niektoré z nich môžu byť používané nekonečne mnohokrát, iné môžu byť spotrebované.

## Magical Gadgets
Kategória, ktorá pridáva rôzne kúzelné zbrane / itemy / komponenty, ktoré môžete použiť aby ste si vycraftili niektoré Magic Itemy alebo na sebaobranu.

## Technical Gadgets 
Kategória plná technických vychytávok, tieto vychytávky vám umožnujú lietať, vidieť v noci, veľmi rýchlo kopať, atd.

## Hotbar Pets 
Kategória, ktorá vám dáva šancu mať "domácich mazlíčkov", ktorý vám dajú určitú špeciálnu schopnosť.

## Exotic Garden - Plants, Fruits, Food & Drinks 
Kategória, ktorá vám umožňuje mať rôzne ovocné stromy, jedlá, džúsy atd.

## Exotic Garden - Magical Plants 
Kategória plná kúzelných stromov

## Tinker Tools 
Kategorie podobná módu Tinkers Construct - Více na http://wiki.softcraft.eu/slime-fun/tinker-tools

## Soft Expansion 
Kategória, ktorá vám prináša nový armor, zbrane a taktiež aquarium
*Tato kategorie je námi vytvořená*

## Lucky Block Expansion : 
Kategória, ktorá vám prináša Lucky Blocky, tieto blocky majú možnosť vám priniesť neštastie ale tak isto aj štastie
*Tato kategorie obsahuje naše a upravené věci*

## Energy & Electricity 
Kategória, ktorá je plná energie, prináša nové stroje, generátory a reaktory, tieto stroje musia byť poháňané elektrinou

## GPS-based Machines 
Kategória, ktorá prináša stroje, ktoré  vám pomôžu sa dostávať z miesta na miesto - teleportácia, ťažba ropy, atd.

## Cargo Management 
Kategória, ktorá prináša stroje, ktoré vám pomôžu premiestňovať veci z miesta na miesto
# Tato stránka není hotová!
## Chceš pomoct v rozvoji této stránky? Napiš na Discord!


*Autoři: JustYusari_, pytroday*