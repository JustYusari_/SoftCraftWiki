<!-- TITLE: Základni příkazy -->
# Teleportace po serveru
**/lobby, /hub** - Teleport na základní server, kde si vyberete jakou minihru si chcete zahrát.
**/survival** - Teleport na Survival server.
**/prison** - Teleport na Prison server.
**/skyblock** - Teleport na Skyblock server.
**/bedwars** - Teleport na BedWars server.
**/vanilla** - Teleport na Vanilla server.
**/kitpvp** - Teleport na KitPvP server.
# Friends
**/fr  list** - Zobrazí seznam všech tvých kamarádů.
**/fr add (nick)** - Odešleš žádost o přátelství.
**/fr accept (nick)** - Přijmeš žádost o přátelství.
**/fr deny (nick)** - Odmítneš žádost o přátelství.
**/fr b (zpráva)** - Odešleš zprávu všem kamarádům kteří jsou online.
**/fr msg (nick) (zpráva)** - Odešleš zprávu kamarádovi.
**/fr jump (nick)** - Připojíš se za kamarádem.
**/fr settings** - Nastavení Friends.
# Clans
**/clan create (Jméno) (Tag)** - Vytvoření clanu.
**/clan delete** - Smažeš svůj clan.
**/clan invite (nick)** - Pozveš hráče do clanu.
**/clan chat (text)** - Napíšeš zprávu do Clan Chatu.
**/clan leave** - Opustíš clan
**/clan tag (Tag)** - Nastavíš nový Clan Tag.
**/clan name (Jméno)** -Nastavíš nové Clan Jméno
**/clan settings** - Nastavení Clanů.
