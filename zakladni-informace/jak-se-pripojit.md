<!-- TITLE: Jak se připojit -->
<!-- SUBTITLE: Rychlý návod jak se připojit na náš minecraft server -->

# Verze
Náš server běží na 1.8-1.13, avšak doporučujeme použít pro hraní verzi **1.12.2**!
# IP Adresa
Na náš server se připojíš pomocí ip adresy: ```play.softcraft.eu```

![Wrok](/uploads/jak-se-pripojit/wrok.png "Wrok")
# Už to skoro máš
Pokud si vše udělal správně, tak už jen stačí se připojit.

![Djvm](/uploads/jak-se-pripojit/djvm.png "Djvm")