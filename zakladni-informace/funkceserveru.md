<!-- TITLE: Funkce Serveru -->
<!-- SUBTITLE: Seznam všech funkcí na našem serveru -->

# Tokeny (Všechny servery)
Tokeny jsou serverová měna, se kterou si budeš moct v blízké budoucnosti zakoupit Kosmetické předměty, Mystery Boxy, Join Messages, Kill Message, Death Message a podobně.

# VoteParty (Survival, Prison, SkyBlock)
Každých 50 hlasů se na serverech Survival, SkyBlock a Prison spustí **"VoteParty"**. Každý online hráč na serveru získá Piston se jménem, ze kterého může dropnout spoustu hezkých věcí.
*Informace: Pokud nic nezískáš do inventáře, tak je možné, že jsi získal klíč nebo money! Klíče jsou použitelné v /crates*
![Crates](/uploads/funkceserveru/crates.png "Crates")

# Upgrade Menu, Click Sell (Prison)
Po novém Updatu Prisonu proběhla velká změna funkcí a s tím přišla obrovské usnadnění. Nyní Upgrade Menu můžeš otevřít **Shift + Pravý Klik** s krumpáčem v ruce a prodávat itemy na tvém Ranku pomocí **pravého kliku** s krumpáčem.
![Prison](/uploads/funkceserveru/prison.png "Prison")

# Aukce (Survival, SkyBlock, Prison)
Pro hodně hráčů známé teritorium, kde si poměrně rychle vydělat pěkný balík nebo se zbavit nějakého bordelu. 
Aukce lze otevřít pomocí příkazu **/ah**
Do aukce můžeš vložit item napsáním příkazu **/ah sell**

# EventSystem (Survival)
Dne 4.8 byl vytvořen EventSystem Majitelem `JustYusari_`, který automatizuje zatím 3 Eventy. **Ore Rush, OneShot a Slaparoo**

## OreRush
V tomto eventu máš za úkol nakopat co nejvíce bodů v jeskyni. Hráč, který za 2 minuty a 30 sekund vytěží nejvíce bodů vyhrál. Hraje se na 3 místa.

## Slaparoo
V tomto eventu máš za úkol shodit 20 hráčů z platformy. První hráč, který získá 20 bodů vyhrál. Hraje se na 3 místa.

## OneShot
V tomto eventu máš za úkol získat 15 killů. Lukem zabiješ hráče na ránu, avšak máš jen 1 pokus, jelikož máš jen jeden šíp. Za každý kill se ti šíp obnoví. Máš také Iron Sword. První hráč s 15 killy vyhrává. Hraje se na 3 místa.
