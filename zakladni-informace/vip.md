<!-- TITLE: Jak získat VIP -->


# Platba
SoftCraft podporuje platbu pomocí **PayPal** a **SMS**.
Platba pomocí paypal: http://softcrafteu.buycraft.net/ 

Pokud chceš platit pomocí **SMS** tak se na našem survivalu teleportuj na **/warp vip** a klikni na **NPC**, poté jen postupuj podle instrukcí.
![Luid](/uploads/luid.png "Luid")
